# 临时仓库 处理软件下架相关事宜

请按照模板的格式提交 issue

issue格式：
```
下架的软件包名：

	xxx.xxxxxx

下架软件的分类（或者spk链接）：

	如：视频播放

下架原因：

	1.xxxxxxx

	2.xxxxxxxxxxxxxx

联系邮箱：

	如：momen@deepinos.org 注：请留上传时使用的邮箱
```

商店相关人员在审核通过后7个工作日内尽快下架，具体进度请留意issue状态。
